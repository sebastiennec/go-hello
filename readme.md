#go-hello
A simple app to test out some concepts in go

## Web framework - Gin https://github.com/gin-gonic/gin
Used for routing, response serialization (json), middleware etc.

## ORM - GORM https://github.com/jinzhu/gorm
Used to query the database and type responses to structs

## Database - Postgresql

## Docker and docker-compose
docker-compose up

Will create a postgres database with seed data, serve a pgadmin4 dashboard on port 5050 and serve this app on port 5000 