\c store;
INSERT INTO catalog.currencies
  (id, currencyCode, name)
VALUES
  ('eur', 'EUR', 'Euro'),
  ('usd', 'USD', 'United States Dollar'),
  ('gbp', 'GBP', 'Great British Pound')
;

INSERT INTO catalog.products
  (id, name, description, tags)
VALUES
  ('test-1', 'Test', 'A test product', '{"test", "product"}'),
  ('test-2', 'Test 2', 'Another test product', '{}')
;

INSERT INTO catalog.bundles
  (id, name)
VALUES
  ('test-bundle-1', '100 Plex Bundle')
;

INSERT INTO catalog.bundleProducts
  (product, bundle, quantity)
VALUES
  ('test-1', 'test-bundle-1', 100),
  ('test-2', 'test-bundle-1', 100)
;

INSERT INTO catalog.prices
  (product, currency, amount)
VALUES
  ('test-1', 'usd', 15.00),
  ('test-1', 'eur', 12.50),
  ('test-1', 'gbp', 10.00),
  ('test-2', 'usd', 6.00),
  ('test-2', 'eur', 5.00),
  ('test-2', 'gbp', 4.00)
;

INSERT INTO catalog.assets
  (product, tag, resource)
VALUES
  ('test-1', 'image', 's3://whatever'),
  ('test-1', 'banner', 's3://whatever'),
  ('test-2', 'image', 's3://whatever'),
  ('test-2', 'banner', 's3://whatever')
;
