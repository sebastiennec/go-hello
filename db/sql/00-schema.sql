-- Super simple catalog database

-- Establish the database and custom schema.
CREATE DATABASE store;
\c store;
CREATE SCHEMA catalog;

-- Currency Table
CREATE TABLE catalog.currencies (
  id TEXT PRIMARY KEY,  -- Identifier, usually resembles the currency code.
  currencyCode TEXT,    -- ISO-4217 currency code (three letters, uppercase).
  name TEXT             -- English name of the currency.
);

-- Products Table
CREATE TABLE catalog.products (
  id TEXT PRIMARY KEY,  -- Identifier, custom tag.
  name TEXT,            -- English name of the product.
  description TEXT,     -- Description of the product.
  tags TEXT[]           -- List of custom text tags related to the product.
);

-- Product Prices Table
CREATE TABLE catalog.prices (
  product TEXT REFERENCES catalog.products NOT NULL,     -- The product the price is associated with.
  currency TEXT REFERENCES catalog.currencies NOT NULL,  -- The currency the price is associated with.
  amount NUMERIC CHECK (amount > 0),                     -- The price in the associated currency.
  PRIMARY KEY(product, currency)
);

-- Product Bundles Table
CREATE TABLE catalog.bundles (
  id TEXT PRIMARY KEY,            -- Custom identifier tag.
  name TEXT                       -- English name of the bundle.
);

-- Bundle to Product Relational Table
CREATE TABLE catalog.bundleProducts (
  product TEXT REFERENCES catalog.products NOT NULL, -- The product itself.
  bundle TEXT REFERENCES catalog.bundles NOT NULL,   -- Bundle to include product in.
  quantity INTEGER CHECK (quantity > 0) DEFAULT 1    -- Quantity of products.
);

-- Product Assets Table
CREATE TABLE catalog.assets (
  product TEXT REFERENCES catalog.products NOT NULL,  -- The product the asset is associate with.
  tag TEXT NOT NULL,                                  -- The custom tag indicating how the asset should be used.
  resource TEXT NOT NULL,                             -- Custom text for the asset, likely pointing to an S3 object.
  PRIMARY KEY(product, tag)
);
