package main

import (
	"github.com/gin-gonic/gin"
)

// ConfigureRoutes ...
func ConfigureRoutes(engine *gin.Engine) {
	SetProductRoutes(engine)
}
