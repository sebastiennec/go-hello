package main

import "github.com/gin-gonic/gin"

// SetProductRoutes ...
// Sets the product routes
func SetProductRoutes(engine *gin.Engine) {
	engine.GET("api/products", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"data": GetProducts(),
		})
	})
}
