package main

// GetProducts ...
// Returns a collection of products
func GetProducts() []Product {
	products := []Product{}
	db.Find(&products)

	return products
}
