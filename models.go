package main

// Piled all of the models for this app in here
// Probably a really bad idea for larger apps...

// Currency ...
type Currency struct {
	ID           string `json:"id"`
	CurrencyCode string `json:"currency-code"`
	Name         string `json:"name"`
}

// Product ...
type Product struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Tags        []string `json:"tags"`
}

// Price ...
type Price struct {
	Product  Product  `json:"id"`
	Currency Currency `json:"currency"`
	Amount   int      `json:"amount"`
}

// Bundle ...
type Bundle struct {
	ID   Product `json:"id"`
	Name string  `json:"name"`
}

// BundleProduct ...
type BundleProduct struct {
	Product  []Product `json:"product"`
	Bundle   Bundle    `json:"bundle"`
	Quantity int16     `json:"quantity"`
}

// Asset ...
// An image or something to be returned with a product
type Asset struct {
	Product  Product `json:"product"`
	Tag      string  `json:"tag"`
	Resource string  `json:"resource"`
}
