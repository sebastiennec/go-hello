FROM golang:1.8

WORKDIR /go/src/hello
COPY . .

# install dependencies
RUN go get -d -v ./...
# deploy the app
RUN go install -v ./...

CMD ["hello"]